#!/bin/bash

# Script to test PCP with sysbench

export DURATION=${DURATION:-1800}
export LAT_THRES=${LAT_THRES:-40}
export SMI_THRES=${SMI_THRES:-40}
export LAT_LOOP=${LAT_LOOP:-1}
export STRESS_NG=${STRESS_NG:-1}
export LOOPS

export nrcpus=$(grep -c ^processor /proc/cpuinfo)
export rhel_major=$(grep -o '[0-9]*\.[0-9]*' /etc/redhat-release | awk -F '.' '{print $1}')

[ -f $TEST ] && TEST="~/rt-tests/latency_perf/rt_cyclic_stress"

loops=1
rate=1
board="R4"
log_file="stress-ng"
test_dir=$(pwd)
load=l

# Get params
# -p    To run sysbench with PCP enable/disabled yes/no value
# -t    Run sysbench cpu or memory test cpu/mem
# -l    Number of loops 
# -f    Pmlogger log file 
# -d    Test directory
# -r    Interval rate
# -d    Duration in seconds
# -w    Load: l (light) m (medium) or h (heavy)

# PCP
start_pcp()
{
    # Start PCP
    systemctl start pmcd
    # Start pmlogger
    systemctl start pmlogger 
    pml_file=${test_dir}/${log_file}_$(date -I)_rate_${rate}
    echo "pml_file is $pml_file "
    pmlogger -c /var/lib/pcp/config/pmlogger/config.default -t $rate  $pml_file &
}

stop_pcp()
{
    # Stop pmlogger
#    systemctl disable pmlogger
    systemctl stop pmlogger
    pkill pmlogger
    # Stop PCP
#    systemctl disable pmcd
    systemctl stop pmcd
}

run_tests()
{
    if [[ $sys_test == "cpu" ]]; then
        # Start cpu test
        echo "Starting cpu tests ..."
        #runtest_cpu
        run_stress-ng
    elif [[ $sys_test == "mem" ]]; then
        # Start memory test
        echo "Starting memory tests ..."
        runtest_mem
    elif [[ $sys_test == "mem_m" ]]; then
        # Start memory test
        echo "Starting memory tests ..."
        runtest_mem_methods
    elif [[ $sys_test == "io" ]]; then
        # Start IO test
        echo "Starting IO tests ..."
        runtest_io
    elif [[ $sys_test == "cpu_mem" ]]; then
        # Start cpu_mem test
        echo "Starting cpu and memory tests ..."
        runtest_cpu_vm
    elif [[ $sys_test == "cpu_mem_io" ]]; then
        # Start cpu_mem_io test
        echo "Starting cpu_mem_io tests ..."
        runtest_cpu_mem_io
    elif [[ $sys_test == "msg" ]]; then
        # Start message queue test
        echo "Starting message_queue tests ..."
        runtest_msg_queue
    elif [[ $sys_test == "cpu_mem_io_msg" ]]; then
        # Start cpu_mem_io_msg  test
        echo "Starting cpu_mem_io_msg tests ..."
        runtest_cpu_mem_io_msg
    elif [[ $sys_test == "net" ]]; then
        # Start network test
        echo "Starting network tests ..."
        runtest_network
    elif [[ $sys_test == "cpu_mem_io_msg_net" ]]; then
        # Start cpu_mem_io_msg_net  test
        echo "Starting cpu_mem_io_msg tests ..."
        runtest_cpu_mem_io_msg_net
    elif [[ $sys_test == "idle" ]]; then
        # Run timerlat on idle system
        echo "Running timerlat on idle system"
        run_timerlat
    else
        echo "Please enter cpu or mem"
    fi
}

run_timerlat()
{
    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u" 
    echo "$cmdline"       
    $cmdline | tee timerlat.out
}

enable_stress_ng()
{
    begin=$1
    end=$2

    if [[ $load == "l" ]];then
        # Low load
        cpu_load=25 
    elif [[ $load == "m" ]];then
        # Medium load
         cpu_load=50
    elif [[ $load == "h" ]];then
        # Heavy load
         cpu_load=80
    else
        echo "Value not recognized $load"
        exit
    fi

echo "begin $begin end $end "
    while [ ${begin} -lt ${end} ]; do
        taskset -c ${begin} stress-ng --cpu 1 --cpu-load ${cpu_load} --cpu-method fft --metrics --timeout 24h &  
        pid_list+=" $!"
        echo -e "pids $pid_list \n" 
       begin=$(( begin + 1 ))
    done
}

run_stress-ng()
{

    if [ $STRESS_NG -eq 1 ]; then
        echo "enable stress-ng for ${cpu_load} load on per $c_low to $c_high"
        enable_stress_ng $c_low $nrcpus
        sleep 60
    fi

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
#    cmdline="taskset -c $cpu_list cyclictest -m -q -p95 -D $DURATION -h40 -i 100 -t $isolate_num -a $cpu_list"
    echo "$cmdline" 
    $cmdline |tee timerlat.out
}

runtest_io()
{
    begin=$c_low
    end=$c_high

    if [[ $load == "l" ]];then
        # Low load
      #  taskset -c $c_low-$c_high stress-ng --iomix 2 -v --timeout 24h &
        stress-ng --iomix 2 -v --timeout 24h --taskset $c_low-$c_high & 

    elif [[ $load == "m" ]];then
        # Medium load
       # taskset -c $c_low-$c_high stress-ng --iomix 4 -v --timeout 24h & 
        stress-ng --iomix 4 -v --timeout 24h --taskset $c_low-$c_high & 

    elif [[ $load == "h" ]];then
        # Heavy load
      #  taskset -c $c_low-$c_high stress-ng --iomix 8 -v --timeout 24h & 
        stress-ng --iomix 8 -v --timeout 24h --taskset $c_low-$c_high & 

    else
        echo "Value not recognized $load"
        exit
    fi

    pid_list+=" $!"
#    pid_list=`echo $(pgrep stress-ng)`
   echo "pids $pid_list" 

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}

runtest_mem()
{

#    if [ $STRESS_NG -eq 1 ]; then
#        echo "enable stress-ng for ${cpu_load} load on per $c_low to $c_high"
#        enable_stress_ng $c_low $nrcpus
#        sleep 60
#    fi

    begin=$c_low
    end=$c_high

    if [[ $load == "l" ]];then
        # Low load
        stress-ng --vm 8 --vm-bytes 25% -v --timeout 24h --taskset $c_low-$c_high & 
    elif [[ $load == "m" ]];then
        # Medium load
        stress-ng --vm 8 --vm-bytes 50% -v --timeout 24h --taskset $c_low-$c_high &
    elif [[ $load == "h" ]];then
        # Heavy load
        stress-ng --vm 8 --vm-bytes 75% -v --timeout 24h --taskset $c_low-$c_high & 
    else
        echo "Value not recognized $load"
        exit
    fi
    pid_list+=" $!"
    echo -e "pids $pid_list \n" 

#    if [[ $load == "l" ]];then
#        # Low load
#        vm_bytes="25%"
#    elif [[ $load == "m" ]];then
#        # Medium load
#        vm_bytes="50%"
#    elif [[ $load == "h" ]];then
#        # Heavy load
#        vm_bytes="75%"
#    else
#        echo "Value not recognized $load"
#        exit
#    fi


#    echo "begin $begin end $end "

#    while [ ${begin} -lt ${end} ]; do
#    #    taskset -c ${begin}  stress-ng --vm 1 --vm-bytes ${vm_bytes} -v --timeout 24h &  
#    stress-ng --vm 1 --vm-bytes ${vm_bytes} -v --timeout 24h --taskset ${begin} &  
#        pid_list+=" $!"
#        echo -e "pids $pid_list \n" 
#       begin=$(( begin + 1 ))
#    done

#echo -e "trying taskset separately \n"
#stress-ng --vm 1 --vm-bytes ${vm_bytes} -v --timeout 24h --taskset 0 &
#stress-ng --vm 1 --vm-bytes ${vm_bytes} -v --timeout 24h --taskset 1 &
#stress-ng --vm 1 --vm-bytes ${vm_bytes} -v --timeout 24h --taskset 2 &
#stress-ng --vm 1 --vm-bytes ${vm_bytes} -v --timeout 24h --taskset 3 &
#stress-ng --vm 1 --vm-bytes ${vm_bytes} -v --timeout 24h --taskset 4 &

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}

runtest_mem_methods()
{
    
    if [[ $load == "l" ]];then
        # Low load
        stress-ng --vm 8 --vm-bytes 25% --metrics --perf --vm-method $method --timeout 24h --taskset $c_low-$c_high & 
    elif [[ $load == "m" ]];then
        # Medium load
        stress-ng --vm 8 --vm-bytes 50% --metrics --perf --vm-method $method --timeout 24h --taskset $c_low-$c_high &
    elif [[ $load == "h" ]];then
        # Heavy load
        stress-ng --vm 8 --vm-bytes 75% --metrics --perf --vm-method $method --timeout 24h --taskset $c_low-$c_high & 
    else
        echo "Value not recognized $load"
        exit
    fi

    pid_list+=" $!"
    echo -e "pids $pid_list \n" 

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}

runtest_cpu_io()
{
    if [[ $load == "l" ]];then
        # Low load
        #stress-ng --iomix 2 --cpu 8 -v --timeout 24h &
        stress-ng --iomix 2 --cpu 8 -v --timeout 24h --taskset $c_low-$c_high & 
    elif [[ $load == "m" ]];then
        # Medium load
        #stress-ng --iomix 4 -v --timeout 24h & 
        stress-ng --iomix 4 --cpu 8 -v --timeout 24h --taskset $c_low-$c_high & 
    elif [[ $load == "h" ]];then
        # Heavy load
        #stress-ng --iomix 8 -v --timeout 24h & 
        stress-ng --iomix 8 --cpu 8 -v --timeout 24h --taskset $c_low-$c_high & 
    else
        echo "Value not recognized $load"
        exit
    fi

    pid_list+=" $!"
    echo -e "pids $pid_list \n" 

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}

runtest_cpu_mem_io()
{
    if [[ $load == "l" ]];then
        # Low load
         stress-ng --iomix 2 --cpu 8 --cpu-load 25 --cpu-method fft --vm 8 --vm-bytes 25% -v --timeout 24h --taskset $c_low-$c_high & 
    elif [[ $load == "m" ]];then
        # Medium load
        stress-ng --iomix 4 --cpu 8 --cpu-load 50 --cpu-method fft --vm 8 --vm-bytes 50% -v --timeout 24h --taskset $c_low-$c_high & 
    elif [[ $load == "h" ]];then
        # Heavy load
        stress-ng --iomix 8 --cpu 8 --cpu-load 80 --cpu-method fft -vm 8 --vm-bytes 75% -v --timeout 24h --taskset $c_low-$c_high & 
    else
        echo "Value not recognized $load"
        exit
    fi

    pid_list+=" $!"
    echo -e "pids $pid_list \n" 

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}

runtest_cpu_vm()
{
    begin=$c_low
    end=$_high

    if [[ $load == "l" ]];then
        # Low load
        stress-ng --vm 8 --vm-bytes 25% --cpu 8 --cpu-load 25 --cpu-method fft -v --timeout 24h --taskset $c_low-$c_high & 

#stress-ng --vm 1 --vm-bytes 25% --cpu 1 --cpu-load 25 --cpu-method fft -v --timeout 24h --taskset 0-0 &
#stress-ng --vm 1 --vm-bytes 25% --cpu 1 --cpu-load 25 --cpu-method fft -v --timeout 24h --taskset 1-1 &
#stress-ng --vm 1 --vm-bytes 25% --cpu 1 --cpu-load 25 --cpu-method fft -v --timeout 24h --taskset 2-2 &
#stress-ng --vm 1 --vm-bytes 25% --cpu 1 --cpu-load 25 --cpu-method fft -v --timeout 24h --taskset 3 &
#stress-ng --vm 1 --vm-bytes 25% --cpu 1 --cpu-load 25 --cpu-method fft -v --timeout 24h --taskset 4 &
#
    elif [[ $load == "m" ]];then
        # Medium load
        stress-ng --vm 8 --vm-bytes 50% --cpu 8 --cpu-load 50 --cpu-method fft -v --timeout 24h --taskset $c_low-$c_high &
    elif [[ $load == "h" ]];then
        # Heavy load
        stress-ng --vm 8 --vm-bytes 75% --cpu 8 --cpu-load 80 --cpu-method fft -v --timeout 24h --taskset $c_low-$c_high & 
    else
        echo "Value not recognized $load"
        exit
    fi

    pid_list+=" $!"
    echo -e "pids $pid_list \n" 

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}

runtest_msg_queue()
{

    begin=$c_low
    end=$_high

    if [[ $load == "l" ]];then
        # Low load
        #taskset -c $c_low-$c_high stress-ng --mq 2 --times --perf --timeout 24h & 
        stress-ng --mq 2 --times --perf --timeout 24h --taskset $c_low-$c_high & 
    elif [[ $load == "m" ]];then
        # Medium load
        #taskset -c $c_low-$c_high stress-ng --mq 4 --times --perf --timeout 24h &
        stress-ng --mq 4 --times --perf --timeout 24h --taskset $c_low-$c_high &
    elif [[ $load == "h" ]];then
        # Heavy load
        #taskset -c $c_low-$c_high stress-ng --mq 8 --times --perf --timeout 24h & 
        stress-ng --mq 8 --times --perf --timeout 24h --taskset $c_low-$c_high & 
    else
        echo "Value not recognized $load"
        exit
    fi

    pid_list+=" $!"
    echo -e "pids $pid_list \n" 

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}

runtest_cpu_mem_io_msg()
{
    if [[ $load == "l" ]];then
        # Low load
        stress-ng --iomix 2 --cpu 8 --cpu-load 25 --cpu-method fft --vm 8 --vm-bytes 25% -v --mq 2 --times --perf --timeout 24h --taskset $c_low-$c_high & 
    elif [[ $load == "m" ]];then
        # Medium load
        stress-ng --iomix 4 --cpu 8 --cpu-load 50 --cpu-method fft --vm 8 --vm-bytes 50% -v --mq 4 --times --perf --timeout 24h --taskset $c_low-$c_high & 
    elif [[ $load == "h" ]];then
        # Heavy load
        stress-ng --iomix 8 --cpu 8 --cpu-load 80 --cpu-method fft -vm 8 --vm-bytes 75% -v --mq 8 --times --perf --timeout 24h --taskset $c_low-$c_high & 
    else
        echo "Value not recognized $load"
        exit
    fi

    pid_list+=" $!"
    echo -e "pids $pid_list \n" 

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}

runtest_network()
{

    begin=$c_low
    end=$_high

    if [[ $load == "l" ]];then
        # Low load
       # taskset -c $c_low-$c_high stress-ng --sock 8 --sock-domain ipv6 --sock-opts sendmsg --metrics --timeout 24h & 
        stress-ng --sock 8 --sock-domain ipv6 --sock-opts sendmsg --metrics --timeout 24h --taskset $c_low-$c_high & 
    elif [[ $load == "m" ]];then
        # Medium load
       # taskset -c $c_low-$c_high stress-ng --sock 16 --sock-domain ipv6 --sock-opts sendmsg --metrics --timeout 24h &
        stress-ng --sock 16 --sock-domain ipv6 --sock-opts sendmsg --metrics --timeout 24h --taskset $c_low-$c_high &
    elif [[ $load == "h" ]];then
        # Heavy load
       # taskset -c $c_low-$c_high stress-ng --sock 32 --sock-domain ipv6 --sock-opts sendmsg --metrics --timeout 24h & 
        stress-ng --sock 32 --sock-domain ipv6 --sock-opts sendmsg --metrics --timeout 24h --taskset $c_low-$c_high & 
    else
        echo "Value not recognized $load"
        exit
    fi

    pid_list+=" $!"
    echo -e "pids $pid_list \n" 

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}

runtest_cpu_mem_io_msg_net()
{
    if [[ $load == "l" ]];then
        # Low load
#         stress-ng --iomix 2 --cpu 8 --cpu-load 25 --cpu-method fft --vm 8 --vm-bytes 25% -v --mq 2 --times --perf --sock 8 --sock-domain ipv6 --sock-opts sendmsg --metrics --timeout 24h & 
         stress-ng --iomix 2 --cpu 8 --cpu-load 25 --cpu-method fft --vm 8 --vm-bytes 25% -v --mq 2 --times --perf --sock 8 --sock-domain ipv6 --sock-opts sendmsg --metrics --timeout 24h --taskset $c_low-$c_high & 
    elif [[ $load == "m" ]];then
        # Medium load
#        stress-ng --iomix 4 --cpu 8 --cpu-load 50 --cpu-method fft --vm 8 --vm-bytes 50% -v --mq 4 --times --perf --sock 16 --sock-domain ipv6 --sock-opts sendmsg --metrics --timeout 24h & 
        stress-ng --iomix 4 --cpu 8 --cpu-load 50 --cpu-method fft --vm 8 --vm-bytes 50% -v --mq 4 --times --perf --sock 16 --sock-domain ipv6 --sock-opts sendmsg --metrics --timeout 24h --taskset $c_low-$c_high & 
    elif [[ $load == "h" ]];then
        # Heavy load
#        stress-ng --iomix 8 --cpu 8 --cpu-load 80 --cpu-method fft -vm 8 --vm-bytes 75% -v --mq 8 --times --perf --sock 32 --sock-domain ipv6 --sock-opts sendmsg --metrics --timeout 24h & 
        stress-ng --iomix 8 --cpu 8 --cpu-load 80 --cpu-method fft -vm 8 --vm-bytes 75% -v --mq 8 --times --perf --sock 32 --sock-domain ipv6 --sock-opts sendmsg --metrics --timeout 24h --taskset $c_low-$c_high & 
    else
        echo "Value not recognized $load"
        exit
    fi

    pid_list+=" $!"
    echo -e "pids $pid_list \n" 

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}

#--- START ---#

# Get arguments
while getopts p:t:l:f:d:r:b:w:m: flag    
do
    case "${flag}" in
        p) pcp=${OPTARG};;
        t) sys_test=${OPTARG};;
        l) loops=${OPTARG};;
	    f) log_file=${OPTARG};;
        d) DURATION=${OPTARG};;
	    r) rate=${OPTARG};;
        b) board=${OPTARG};;
        w) load=${OPTARG};;
        m) method=${OPTARG};;
    esac
done
echo "PCP: $pcp";
echo "Test: $sys_test";
echo "Loops: $loops";
echo "Log file: $log_file";
echo "Duration in secs: $DURATION";
#echo "Test directory: $test_dir";
echo "Interval rate: $rate";
echo "Load : $load"
echo "Board: $board";
echo "Method: $method";

nrcpus=8
if [[ $board == "QR3" ]]; then
# Run on upper 4 CPUs    
    if [ $nrcpus -lt 4 ]; then
        echo "recommend running measure process on >= 4 CPUs machine"
        exit 0
    else
        c_low=$(( nrcpus / 2 ))
        c_high=$(( nrcpus - 1 ))
        cpu_list=$c_low"-"$c_high
        isolate_num=$(( c_high - c_low + 1 ))
    fi
else
    c_low=0
    c_high=7
    cpu_list="0-7"
    isolate_num=8
fi

# Run tests $loop times

x=1
cur_dir=$(pwd)
while [ $x -le $loops ]
do
    # Create new test directory
    echo -e "\n ***** Starting test number $x *****\n"
    cd $cur_dir
    echo "cur_dir: $cur_dir"

    if [[ $pcp == "n" ]]; then
        test_dir=${cur_dir}/${log_file}_no_pcp_$x
    else
        test_dir=${cur_dir}/${log_file}_r${rate}_$x
    fi
    echo "Creating dir: $test_dir"
    mkdir $test_dir
    cd $test_dir
    echo "Current directory: $(pwd)"
    cp /proc/interrupts interrupts-before
    # PCP
    if [[ $pcp == "n" ]]; then
        # Stop PCP and pmlogger
        echo "Stop PCP and pmlogger"
        stop_pcp
## Get mem/cpu info when PCP is not running
#top -d 1 -b|grep "load average" -A 15 >> top.log &
    else
        # Start PCP and pmlogger
        echo "Start PCP and pmlogger"
        start_pcp
        sleep 5
    fi

    run_tests
    sleep 5
    echo -e "Stopping stress-ng"
    kill ${pid_list}
    pid_list=""    
    pkill stress-ng  # Should not find processes

    echo -e "Stopping PCP \n"
    stop_pcp
    cp /proc/interrupts interrupts-after
    x=$((x + 1))

    sleep 60
done
exit 0

