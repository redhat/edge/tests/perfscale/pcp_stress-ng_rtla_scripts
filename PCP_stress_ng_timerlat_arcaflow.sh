#!/bin/bash

# Script to test PCP with stress-ng 
# Podman updates based on Dustin's modified PCP_sysbench.sh script

export DURATION=${DURATION:-1800}
export LAT_THRES=${LAT_THRES:-40}
export SMI_THRES=${SMI_THRES:-40}
export LAT_LOOP=${LAT_LOOP:-1}
export STRESS_NG=${STRESS_NG:-1}
export LOOPS

export nrcpus=$(grep -c ^processor /proc/cpuinfo)
export rhel_major=$(grep -o '[0-9]*\.[0-9]*' /etc/redhat-release | awk -F '.' '{print $1}')

[ -f $TEST ] && TEST="~/rt-tests/latency_perf/rt_cyclic_stress"

loops=1
rate=1
board="R4"
log_file="stress-ng"
test_dir=$(pwd)
load=l

# Default pcp metric value if none is provided
metric="kernel.uname, hinv.ncpu, mem.physmem, disk.dev.scheduler, kernel.cpu.util.user, kernel.cpu.util.nice, kernel.cpu.util.sys, kernel.cpu.util.wait, kernel.cpu.util.steal, kernel.cpu.util.idle, kernel.percpu.cpu.vuser, kernel.percpu.cpu.nice, kernel.percpu.cpu.sys, kernel.percpu.cpu.wait, kernel.percpu.cpu.steal, kernel.percpu.cpu.idle, disk.all.total, disk.all.read, disk.all.write, disk.all.blkread, disk.all.blkwrite, mem.freemem, mem.util.available, mem.util.used, mem.util.bufmem, mem.util.cached, mem.util.active, mem.util.inactive, mem.util.dirty, swap.in, swap.pagesin, swap.out, swap.pagesout, network.interface.in.packets, network.interface.out.packets, network.interface.in.bytes, network.interface.out.bytes"

# Pre-pull the Arcaflow plugin containers
pcp_plugin="quay.io/arcalot/arcaflow-plugin-pcp:0.9.0"
stressng_plugin="quay.io/arcalot/arcaflow-plugin-stressng:0.6.0"
for i in $pcp_plugin $stressng_plugin; do
	podman pull $i
done


# Get params
# -p    To run sysbench with PCP enable/disabled yes/no value
# -t    Run sysbench cpu or memory test cpu/mem
# -l    Number of loops 
# -f    Pmlogger log file 
# -d    Test directory
# -r    Interval rate
# -d    Duration in seconds
# -w    Load: l (light) m (medium) or h (heavy)


# Define the pcp plugin input as YAML
pcp_in=$(cat << EOM
pmrep_conf: |
$(curl https://gitlab.com/redhat/edge/tests/perfscale/pts/-/raw/main/pcp2json.conf?ref_type=heads | grep -v ^# | sed s/^/\ \ /g)
pmlogger_interval: 0.25
pmlogger_metrics: $metric
generate_csv: True
flatten: True
EOM)

# PCP
start_pcp()
{
    # Start PCP
    PCP_PIDFILE="/tmp/pcp_pid_$(tr -dc A-Za-z0-9 </dev/urandom | head -c 6)"
    # This version of the command displays and redirects stderr of the pcp container
    #echo "$pcp_in" | podman run --privileged --network host --read-only=false --pidfile $PCP_PIDFILE -i --rm $pcp_plugin --debug -f - > $test_dir/${log_file}_$(date -I)_rate_${rate}.yaml 2> >(tee $test_dir/${log_file}_$(date -I)_rate_${rate}.csv >&2) &
    # This version of the command only redirects stderr of the pcp container
    echo "$pcp_in" | podman run --privileged --network host --read-only=false --pidfile $PCP_PIDFILE -i --rm $pcp_plugin --debug -f - > $test_dir/${log_file}_$(date -I)_rate_${rate}.yaml 2> $test_dir/${log_file}_$(date -I)_rate_${rate}.csv &
}

stop_pcp()
{
    # Stop pmlogger
    if [[ -f $PCP_PIDFILE ]]; then 
        kill -2 $(cat $PCP_PIDFILE) 
    fi
}

run_tests()
{
    if [[ $sys_test == "cpu" ]]; then
        # Start cpu test
        echo "Starting cpu tests ..."
        #runtest_cpu
        run_stress-ng
    elif [[ $sys_test == "mem" ]]; then
        # Start memory test
        echo "Starting memory tests ..."
        runtest_mem
    elif [[ $sys_test == "mem_m" ]]; then
        # Start memory test
        echo "Starting memory tests ..."
        runtest_mem_methods
    elif [[ $sys_test == "io" ]]; then
        # Start IO test
        echo "Starting IO tests ..."
        runtest_io
    elif [[ $sys_test == "cpu_mem" ]]; then
        # Start cpu_mem test
        echo "Starting cpu and memory tests ..."
        runtest_cpu_vm
    elif [[ $sys_test == "cpu_mem_io" ]]; then
        # Start cpu_mem_io test
        echo "Starting mixed tests ..."
        runtest_cpu_mem_io
    elif [[ $sys_test == "msg" ]]; then
        # Start message queue test
        echo "Starting message_queue tests ..."
        runtest_msg_queue
    elif [[ $sys_test == "cpu_mem_io_msg" ]]; then
        # Start cpu_mem_io_msg  test
        echo "Starting cpu_mem_io_msg tests ..."
        runtest_cpu_mem_io_msg
    elif [[ $sys_test == "net" ]]; then
        # Start network test
        echo "Starting network tests ..."
        runtest_network
    elif [[ $sys_test == "cpu_mem_io_msg_net" ]]; then
        # Start cpu_mem_io_msg_net  test
        echo "Starting cpu_mem_io_msg tests ..."
        runtest_cpu_mem_io_msg_net
    elif [[ $sys_test == "idle" ]]; then
        # Run timerlat on idle system
        echo "Running timerlat on idle system"
        run_timerlat
    else
        echo "Please enter cpu or mem"
    fi
}

run_timerlat()
{
    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u" 
    echo "$cmdline"       
    $cmdline | tee timerlat.out
}

enable_stress_ng()
{
    begin=$1
    end=$2

    if [[ $load == "l" ]];then
        # Low load
        cpu_load=25 
    elif [[ $load == "m" ]];then
        # Medium load
         cpu_load=50
    elif [[ $load == "h" ]];then
        # Heavy load
         cpu_load=80
    else
        echo "Value not recognized $load"
        exit
    fi

cpu_in=$(cat << EOM
timeout: 86400
metrics-brief: true
stressors:
    - stressor: cpu
      workers: 8
      cpu-method: fft
      cpu-load: ${cpu_load}
EOM)

    echo "$cpu_in"  | taskset -c ${cpu_list} podman run --read-only=false -i --rm $stressng_plugin  --debug -f - > stressng_cpu.yaml 2> >(tee stressng_cpu.stderr >&2) &

}

run_stress-ng()
{

    if [ $STRESS_NG -eq 1 ]; then
        echo "enable stress-ng for ${cpu_load} load on per $c_low to $c_high"
        enable_stress_ng $c_low $nrcpus
        sleep 60
    fi

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
#    cmdline="taskset -c $cpu_list cyclictest -m -q -p95 -D $DURATION -h40 -i 100 -t $isolate_num -a $cpu_list"
    echo "$cmdline" 
        $cmdline |tee timerlat.out
}

runtest_mem()
{
    if [[ $load == "l" ]];then
        # Low load
        vm_bytes="25%"
    elif [[ $load == "m" ]];then
        # Medium load
        vm_bytes="50%"
    elif [[ $load == "h" ]];then
        # Heavy load
        vm_bytes="75%"
    else
        echo "Value not recognized $load"
        exit
    fi

mem_in=$(cat << EOM
timeout: 86400
metrics-brief: true
stressors:
    - stressor: vm
      workers: 8
      vm-bytes: ${vm_bytes}
EOM)

    echo "$mem_in"  | taskset -c ${cpu_list} podman run --read-only=false -i --rm $stressng_plugin  --debug -f - > stressng_mem.yaml 2> >(tee stressng_mem.stderr >&2) &

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out

}

runtest_mem_methods()
{
    if [[ $load == "l" ]];then
        # Low load
        vm_bytes="25%"
    elif [[ $load == "m" ]];then
        # Medium load
        vm_bytes="50%"
    elif [[ $load == "h" ]];then
        # Heavy load
        vm_bytes="75%"
    else
        echo "Value not recognized $load"
        exit
    fi

mem_in=$(cat << EOM
timeout: 86400
metrics-brief: true
stressors:
    - stressor: vm
      workers: 8
      vm-bytes: ${vm_bytes}
      vm-method: ${method}
EOM)

    echo "$mem_in"  | taskset -c ${cpu_list} podman run --read-only=false -i --rm $stressng_plugin  --debug -f - > stressng_mem_m.yaml 2> >(tee stressng_mem_m.stderr >&2) &

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}

runtest_io()
{
    if [[ $load == "l" ]];then
        # Low load
        iomix=2
       # stress-ng --iomix 2 -v --timeout 24h & 
    elif [[ $load == "m" ]];then
        # Medium load
        iomix=4
       # stress-ng --iomix 4 -v --timeout 24h & 
    elif [[ $load == "h" ]];then
        # Heavy load
        iomix=8
       # stress-ng --iomix 8 -v --timeout 24h & 
    else
        echo "Value not recognized $load"
        exit
    fi

io_in=$(cat << EOM
timeout: 86400
metrics-brief: true
stressors:
    - stressor: iomix
      workers: ${iomix}
EOM)

    echo "$io_in"  | taskset -c ${cpu_list} podman run --read-only=false -i --rm $stressng_plugin  --debug -f - > stressng_io.yaml 2> >(tee stressng_io.stderr >&2) &


    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}

runtest_msg_queue()
{
    if [[ $load == "l" ]];then
        # Low load
        # stress-ng --mq 2 --times --perf --timeout 24h & 
        mq=2
    elif [[ $load == "m" ]];then
        # Medium load
        #stress-ng --mq 4 --times --perf --timeout 24h &
        mq=4
    elif [[ $load == "h" ]];then
        # Heavy load
        #stress-ng --mq 8 --times --perf --timeout 24h & 
        mq=8
    else
        echo "Value not recognized $load"
        exit
    fi

msg_queue_in=$(cat << EOM
timeout: 86400
metrics-brief: true
stressors:
    - stressor: mq
      workers: ${mq}
EOM)

    echo "$msg_queue_in"  | taskset -c ${cpu_list} podman run --read-only=false -i --rm $stressng_plugin  --debug -f - > stressng_mq.yaml 2> >(tee stressng_mq.stderr >&2) &

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}

runtest_network()
{
    if [[ $load == "l" ]];then
        # Low load
        sock=8
       # stress-ng --sock 8 --sock-domain ipv6 --sock-opts sendmsg --metrics --timeout 24h & 
    elif [[ $load == "m" ]];then
        # Medium load
        sock=16
        # stress-ng --sock 16 --sock-domain ipv6 --sock-opts sendmsg --metrics --timeout 24h &
    elif [[ $load == "h" ]];then
        # Heavy load
        sock=32
        #stress-ng --sock 32 --sock-domain ipv6 --sock-opts sendmsg --metrics & 
    else
        echo "Value not recognized $load"
        exit
    fi

sock_in=$(cat << EOM
timeout: 86400
metrics-brief: true
stressors:
    - stressor: sock
      workers: ${sock}
      sock_domain=ipv6
      sock-opts=sendmsg

EOM)

    echo "$sock_in"  | taskset -c ${cpu_list} podman run --read-only=false -i --rm $stressng_plugin  --debug -f - > stressng_sock.yaml 2> >(tee stressng_sock.stderr >&2) &

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}

runtest_cpu_vm()
{
    if [[ $load == "l" ]];then
        # Low load
        cpu_load=25
        vm_bytes=25%
    elif [[ $load == "m" ]];then
        # Medium load
        cpu_load=50
        vm_bytes=50%
    elif [[ $load == "h" ]];then
        # Heavy load
        cpu_load=75
        vm_bytes=75%
    else
        echo "Value not recognized $load"
        exit
    fi

cpu_mem_in=$(cat << EOM
timeout: 86400
metrics-brief: true
verbose: true
stressors:
    - stressor: cpu
      workers: 8
      cpu-method: fft
      cpu-load: ${cpu_load}
    - stressor: vm
      workers: 8
      vm-bytes: ${vm_bytes}
EOM)

    echo "$cpu_mem_in"  | taskset -c ${cpu_list} podman run --read-only=false -i --rm $stressng_plugin  --debug -f - > stressng_cpu_mem.yaml 2> >(tee stressng_cpu_mem.stderr >&2) &

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}

runtest_cpu_io()
{
    if [[ $load == "l" ]];then
        # Low load
        cpu_load=25
        iomix=2
        # stress-ng --iomix 2 --cpu 8 -v --timeout 24h & 
    elif [[ $load == "m" ]];then
        # Medium load
        cpu_load=50 
        iomix=4
       # stress-ng --iomix 4 --cpu 8 -v --timeout 24h & 
    elif [[ $load == "h" ]];then
        # Heavy load
        cpu_load=75
        iomix=8
       # stress-ng --iomix 8 --cpu 8 -v --timeout 24h & 
    else
        echo "Value not recognized $load"
        exit
    fi

cpu_io_in=$(cat << EOM
timeout: 86400
metrics-brief: true
verbose: true
stressors:
    - stressor: cpu
      workers: 8
      cpu-method: fft
      cpu-load: ${cpu_load}
    - stressor: iomix
      workers: ${iomix}
EOM)
    echo "$cpu_io_in"  | taskset -c ${cpu_list} podman run --read-only=false -i --rm $stressng_plugin  --debug -f - > stressng_cpu_io.yaml 2> >(tee stressng_cpu_io.stderr >&2) &

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}

runtest_cpu_mem_io()
{
    if [[ $load == "l" ]];then
        # Low load
        cpu_load=25
        vm_bytes=25%
        iomix=2
        # stress-ng --iomix 2 --cpu 8 --cpu-load 25 --cpu-method fft --vm 8 --vm-bytes 25% -v --timeout 24h & 
    elif [[ $load == "m" ]];then
        # Medium load
        cpu_load=50
        vm_bytes=50%
        iomix=4
#        stress-ng --iomix 4 --cpu 8 --cpu-load 50 --cpu-method fft --vm 8 --vm-bytes 50% -v --timeout 24h & 
    elif [[ $load == "h" ]];then
        # Heavy load
        cpu_load=75
        vm_bytes=75%
        iomix=8
#        stress-ng --iomix 8 --cpu 8 --cpu-load 80 --cpu-method fft -vm 8 --vm-bytes 75% -v --timeout 24h & 
    else
        echo "Value not recognized $load"
        exit
    fi


cpu_mem_io_in=$(cat << EOM
timeout: 86400
metrics-brief: true
stressors:
    - stressor: cpu
      workers: 8
      cpu-method: fft
      cpu-load: ${cpu_load}
    - stressor: vm
      workers: 8
      vm-bytes: ${vm_bytes}
    - stressor: iomix
      workers: ${iomix}
EOM)

    echo "$cpu_mem_io_in"  | taskset -c ${cpu_list} podman run --read-only=false -i --rm $stressng_plugin  --debug -f - > stressng_cpu_mem_io.yaml 2> >(tee stressng_cpu_mem_io.stderr >&2) &


    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}

runtest_cpu_mem_io_msg()
{
    if [[ $load == "l" ]];then
        # Low load
        cpu_load=25
        vm_bytes=25%
        iomix=2
        mq=2
 #        stress-ng --iomix 2 --cpu 8 --cpu-load 25 --cpu-method fft --vm 8 --vm-bytes 25% -v --mq 2 --times --perf --timeout 24h & 
    elif [[ $load == "m" ]];then
        # Medium load
        cpu_load=50
        vm_bytes=50%
        iomix=4
        mq=4
#        stress-ng --iomix 4 --cpu 8 --cpu-load 50 --cpu-method fft --vm 8 --vm-bytes 50% -v --mq 4 --times --perf --timeout 24h & 
    elif [[ $load == "h" ]];then
        # Heavy load
        cpu_load=75
        vm_bytes=75%
        iomix=8
        mq=8
     #   stress-ng --iomix 8 --cpu 8 --cpu-load 80 --cpu-method fft -vm 8 --vm-bytes 75% -v --mq 8 --times --perf --timeout 24h & 
    else
        echo "Value not recognized $load"
        exit
    fi

cpu_mem_io_msg_in=$(cat << EOM
timeout: 86400
metrics-brief: true
stressors:
    - stressor: cpu
      workers: 8
      cpu-method: fft
      cpu-load: ${cpu_load}
    - stressor: vm
      workers: 8
      vm-bytes: ${vm_bytes}
    - stressor: iomix
      workers: ${iomix}
    - stressor: mq
      workers: ${mq}
EOM)

    echo "$cpu_mem_io_msg_in"  | taskset -c ${cpu_list} podman run --read-only=false -i --rm $stressng_plugin  --debug -f - > stressng_cpu_mem_io_msg.yaml 2> >(tee stressng_cpu_mem_io_msg.stderr >&2) &

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}



runtest_cpu_mem_io_msg_net()
{
    if [[ $load == "l" ]];then
        # Low load
        cpu_load=25
        vm_bytes=25%
        iomix=2
        mq=2
        sock=8
       #  stress-ng --iomix 2 --cpu 8 --cpu-load 25 --cpu-method fft --vm 8 --vm-bytes 25% -v --mq 2 --times --perf --sock 8 --sock-domain ipv6 --sock-opts sendmsg --metrics --timeout 24h & 
    elif [[ $load == "m" ]];then
        # Medium load
        cpu_load=50
        vm_bytes=50%
        iomix=4
        sock=16
   #     stress-ng --iomix 4 --cpu 8 --cpu-load 50 --cpu-method fft --vm 8 --vm-bytes 50% -v --mq 4 --times --perf --sock 16 --sock-domain ipv6 --sock-opts sendmsg --metrics --timeout 24h & 
    elif [[ $load == "h" ]];then
        # Heavy load
        cpu_load=75
        vm_bytes=75%
        iomix=8
        sock=32
    #    stress-ng --iomix 8 --cpu 8 --cpu-load 80 --cpu-method fft -vm 8 --vm-bytes 75% -v --mq 8 --times --perf --sock 32 --sock-domain ipv6 --sock-opts sendmsg --metrics --timeout 24h & 
    else
        echo "Value not recognized $load"
        exit
    fi

cpu_mem_io_msg_net_in=$(cat << EOM
timeout: 86400
metrics-brief: true
stressors:
    - stressor: cpu
      workers: 8
      cpu-method: fft
      cpu-load: ${cpu_load}
    - stressor: vm
      workers: 8
      vm-bytes: ${vm_bytes}
    - stressor: iomix
      workers: ${iomix}
    - stressor: mq
      workers: ${mq}
    - stressor: sock
      workers: ${sock}
      sock-domain=ipv6
      sock-opts=sendmsg
EOM)

    echo "$cpu_mem_io_msg_net_in"  | taskset -c ${cpu_list} podman run --read-only=false -i --rm $stressng_plugin  --debug -f - > stressng_cpu_mem_io_msg_net.yaml 2> >(tee stressng_cpu_mem_io_msg_net.stderr >&2) &

    echo "use rtla-timerlat to measure system latency"
    cmdline="rtla timerlat hist -d $DURATION -u " 
    echo "$cmdline" 
    $cmdline | tee timerlat.out
}

#--- START ---#

# Get arguments
while getopts p:t:l:f:d:r:b:w:m: flag    
do
    case "${flag}" in
        p) pcp=${OPTARG};;
        t) sys_test=${OPTARG};;
        l) loops=${OPTARG};;
	    f) log_file=${OPTARG};;
        d) DURATION=${OPTARG};;
	    r) rate=${OPTARG};;
        b) board=${OPTARG};;
        w) load=${OPTARG};;
        m) method=${OPTARG};;
    esac
done
echo "PCP: $pcp";
echo "Test: $sys_test";
echo "Loops: $loops";
echo "Log file: $log_file";
echo "Duration in secs: $DURATION";
#echo "Test directory: $test_dir";
echo "Interval rate: $rate";
echo "Load : $load"
echo "Board: $board";
echo "Method: $method";

nrcpus=8
if [[ $board == "QR3" ]]; then
# Run on upper 4 CPUs    
    if [ $nrcpus -lt 4 ]; then
        echo "recommend running measure process on >= 4 CPUs machine"
        exit 0
    else
        c_low=$(( nrcpus / 2 ))
        c_high=$(( nrcpus - 1 ))
        cpu_list=$c_low"-"$c_high
        isolate_num=$(( c_high - c_low + 1 ))
    fi
else
    c_low=0
    c_high=7
    cpu_list="0-7"
    isolate_num=8
fi

# Run tests $loop times

x=1
cur_dir=$(pwd)
while [ $x -le $loops ]
do
    # Create new test directory
    echo -e "\n ***** Starting test number $x *****\n"
    cd $cur_dir
    echo "cur_dir: $cur_dir"

    if [[ $pcp == "n" ]]; then
        test_dir=${cur_dir}/${log_file}_no_pcp_$x
    else
        test_dir=${cur_dir}/${log_file}_r${rate}_$x
    fi
    echo "Creating dir: $test_dir"
    mkdir $test_dir
    cd $test_dir
    echo "Current directory: $(pwd)"
    cp /proc/interrupts interrupts-before
    # PCP
    if [[ $pcp == "n" ]]; then
        # Stop PCP and pmlogger
        echo "Stop PCP and pmlogger"
        stop_pcp
    else
        # Start PCP and pmlogger
        echo "Start PCP and pmlogger"
        start_pcp
        sleep 5
    fi

    run_tests
    sleep 5
    echo "Stopping PCP"
    pkill stress-ng
    stop_pcp
    cp /proc/interrupts interrupts-after
    x=$((x + 1))
    sleep 60
done
exit 0

